extern string_equals
global find_word

section .text

; rdi - указать на строчку которую ищем
; rsi - указатель на начало словаря
find_word:
  .loop:
    mov rdx, rsi
    add rsi, 8
    call string_equals
    cmp rax, 1
    je .find
    xor r9, r9
    cmp [rdx], r9
    je .not_find
    mov rsi, [rdx]
    jmp .loop
  .find:
    mov rax, rsi
    ret
  .not_find:
    mov rax, 0
    ret
