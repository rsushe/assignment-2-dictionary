%include "lib.inc"
%include "words.inc"
extern find_word
%define MAX_LENGTH 256

section .bss
word_buf: resb MAX_LENGTH

section .rodata
not_find: db "input key not found in dictionary", 0
too_long_input_message: db "input message length must be less 256", 0

section .text

global _start

print_string_and_exit:
  call print_string
  call print_newline
  call exit
  
_start:
  mov rdi, word_buf
  mov rsi, MAX_LENGTH
  call read_word
  cmp rax, 0
  je .too_long_input
  
  push rdx
  
  mov rdi, word_buf
  mov rsi, LABEL
  call find_word
  cmp rax, 0
  je .not_find
  
  pop rdi
  add rdi, rax
  inc rdi
  mov rsi, 1
  call print_string_and_exit
  
  .not_find:
     mov rdi, not_find
     mov rsi, 2
     call print_string_and_exit
  .too_long_input:
    mov rdi, too_long_input_message
    mov rsi, 2
    call print_string_and_exit
